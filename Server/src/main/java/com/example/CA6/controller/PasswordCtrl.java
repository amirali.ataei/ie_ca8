package com.example.CA6.controller;

import com.example.CA6.model.JwtResponse;
import com.example.CA6.service.Student;
import com.example.CA6.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static com.example.CA6.util.Tools.studentId;

@RestController
@CrossOrigin
@RequestMapping(value = "/password", produces = MediaType.APPLICATION_JSON_VALUE)
public class PasswordCtrl {
    @Autowired
    private JwtUtil jwtUtil;
    @PostMapping
    public JwtResponse setPassword(@RequestAttribute(name = "User") Student student) {
        String jwtToken = jwtUtil.generateToken(student);
        studentId = student.getStudentId();
        return new JwtResponse(jwtToken);
    }
}
