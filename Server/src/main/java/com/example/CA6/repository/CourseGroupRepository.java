package com.example.CA6.repository;

import com.example.CA6.service.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CourseGroupRepository extends Repository<Course, String>{
    private static final String TABLE_NAME = "CourseGroup";
    private static CourseGroupRepository instance;
    private static final String COURSES_JOIN = "Course OUTER JOIN " +
            "(CourseGroup OUTER JOIN " +
            "(ClassTime OUTER JOIN ExamTime ON " +
            "ClassTime.code = ExamTime.code " +
            "AND ClassTime.classCode = ExamTime.classCode) " +
            "ON CourseGroup.code = ClassTime.code AND" +
            " CourseGroup.classCode = ClassTime.classCode) " +
            "ON Course.code = CourseGroup.code";

    public static CourseGroupRepository getInstance() {
        if (instance == null) {
            try {
                instance = new CourseGroupRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in CourseRepository.create query.");
            }
        }
        return instance;
    }

    private CourseGroupRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s (\n" +
                        "  `code` char(225) NOT NULL,\n" +
                        "  `classCode` char(225) NOT NULL,\n" +
                        "  `instructor` char(225) DEFAULT NULL,\n" +
                        "  `capacity` int DEFAULT NULL,\n" +
                        "  `signedUp` int DEFAULT NULL,\n" +
                        "  `wCapacity` int DEFAULT NULL,\n" +
                        "  `waitings` int DEFAULT NULL,\n" +
                        "  PRIMARY KEY (`code`,`classCode`),\n" +
                        "  CONSTRAINT `coursegroup_ibfk_1` FOREIGN KEY (`code`) REFERENCES `Course` (`code`) ON DELETE CASCADE\n" +
                        ");", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }


    @Override
    protected String getFindByIdStatement() {
        return null;
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {

    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s(code, classCode, instructor, capacity, signedUp, wCapacity, waitings)" +
                "SELECT ?, ?, ?, ?, ?, ?, ? WHERE NOT EXISTS(Select * from %s where code = ? AND classCode = ?);", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Course data) throws SQLException {
        st.setString(1, data.getCode());
        st.setString(2, data.getClassCode());
        st.setString(3, data.getInstructor());
        st.setInt(4, data.getCapacity());
        st.setInt(5, data.getSignedUp());
        st.setInt(6, 0);
        st.setInt(7, 0);
        st.setString(8, data.getCode());
        st.setString(9, data.getClassCode());
    }

    @Override
    protected String getFindAllStatement() {
        return null;
    }

    @Override
    protected void fillFindBySearchTypeValues(PreparedStatement st, String search, String type) throws SQLException {

    }

    @Override
    protected Course convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return null;
    }

    @Override
    protected String getFindBySearchType() {
        return null;
    }

    @Override
    protected ArrayList<Course> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        return null;
    }

    @Override
    protected void fillInsertByIdValues(PreparedStatement st, Course obj, String id) throws SQLException {

    }

    @Override
    protected String getFindBySearch() {
        return null;
    }

    @Override
    protected void fillFindBySearchValues(PreparedStatement st, String search) throws SQLException {

    }

    @Override
    protected void fillFindByTermValues(PreparedStatement st, String id) {

    }

    @Override
    protected String getFindByTermStatement() {
        return null;
    }

    @Override
    protected void fillFindByEmailValues(PreparedStatement st, String email) throws SQLException {

    }

    @Override
    protected String getFindByEmailStatement() {
        return null;
    }
}
