package com.example.CA6.repository;

import com.example.CA6.service.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CourseRepository extends Repository<Course, String>{
    private static final String TABLE_NAME = "Course";
    private static CourseRepository instance;
    private static final String COURSES_JOIN = "Course, " +
            "CourseGroup, " +
            "ClassTime, " +
            "ExamTime " +
            "WHERE " +
            "Course.code = CourseGroup.code And " +
            "CourseGroup.classCode = ClassTime.classCode And " +
            "CourseGroup.classCode = ClassTime.classCode And " +
            "ClassTime.code = ExamTime.code And " +
            "ClassTime.classCode = ExamTime.classCode ";

    public static CourseRepository getInstance() {
        if (instance == null) {
            try {
                instance = new CourseRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in CourseRepository.create query.");
            }
        }
        return instance;
    }

    private CourseRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s (\n" +
                        "  `code` char(225) NOT NULL,\n" +
                        "  `name` char(225) DEFAULT NULL,\n" +
                        "  `units` int DEFAULT NULL,\n" +
                        "  `cType` char(225) DEFAULT NULL,\n" +
                        "  PRIMARY KEY (`code`)\n" +
                        ");", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindBySearchType() {
        return String.format("SELECT " +
                "Course.name, " +
                "Course.code, " +
                "CourseGroup.classCode, " +
                "CourseGroup.instructor, " +
                "Course.units, " +
                "Course.cType, " +
                "CourseGroup.capacity, " +
                "CourseGroup.signedUp, " +
                "ClassTime.time, " +
                "ClassTime.day1, " +
                "ClassTime.day2, " +
                "ClassTime.day3, " +
                "ExamTime.eStart, " +
                "ExamTime.eEnd" +
                " FROM %s AND Course.cType = ? AND Course.name LIKE %%?%% OR CourseGroup.instructor LIKE %%?%%);", COURSES_JOIN);
    }

    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT " +
                "Course.name, " +
                "Course.code, " +
                "CourseGroup.classCode, " +
                "CourseGroup.instructor, " +
                "Course.units, " +
                "Course.cType, " +
                "CourseGroup.capacity, " +
                "CourseGroup.signedUp, " +
                "ClassTime.time, " +
                "ClassTime.day1, " +
                "ClassTime.day2, " +
                "ClassTime.day3, " +
                "ExamTime.eStart, " +
                "ExamTime.eEnd" +
                " FROM %s AND Course.code = ? AND CourseGroup.classCode = ?;", COURSES_JOIN);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id.split("-")[0]);
        st.setString(2, id.split("-")[1]);
    }

    @Override
    protected void fillFindBySearchTypeValues(PreparedStatement st, String search, String type) throws SQLException {
        st.setString(1, type);
        st.setString(2, search);
        st.setString(3, search);
    }

    @Override
    protected String getInsertStatement() {
        return String.format(
                "insert into %s (code, name, units, cType)\n" +
                        " Select ?, ?, ?, ? Where not exists(select * from %s where code=?)", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Course data) throws SQLException {
        st.setString(1, data.getCode());

        st.setString(2, data.getName());
        st.setInt(3, data.getUnits());
        st.setString(4, data.getType());
        st.setString(5, data.getCode());
    }

    @Override
    protected String getFindAllStatement() {
        return String.format("SELECT " +
                "Course.name, " +
                "Course.code, " +
                "CourseGroup.classCode, " +
                "CourseGroup.instructor, " +
                "Course.units, " +
                "Course.cType, " +
                "CourseGroup.capacity, " +
                "CourseGroup.signedUp, " +
                "ClassTime.time, " +
                "ClassTime.day1, " +
                "ClassTime.day2, " +
                "ClassTime.day3, " +
                "ExamTime.eStart, " +
                "ExamTime.eEnd" +
                " FROM %s;", COURSES_JOIN);
    }

    @Override
    protected Course convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new Course(rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getInt(5),
                rs.getString(6),
                rs.getInt(7),
                rs.getInt(8),
                rs.getString(9),
                rs.getString(10),
                rs.getString(11),
                rs.getString(12),
                rs.getString(13),
                rs.getString(14));
    }

    @Override
    protected ArrayList<Course> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Course> courses = new ArrayList<>();
        while (rs.next()) {
            courses.add(this.convertResultSetToDomainModel(rs));
        }
        return courses;
    }

    @Override
    protected void fillInsertByIdValues(PreparedStatement st, Course obj, String id) throws SQLException {

    }

    @Override
    protected String getFindBySearch() {
        return String.format("SELECT " +
                "Course.name, " +
                "Course.code, " +
                "CourseGroup.classCode, " +
                "CourseGroup.instructor, " +
                "Course.units, " +
                "Course.cType, " +
                "CourseGroup.capacity, " +
                "CourseGroup.signedUp, " +
                "ClassTime.time, " +
                "ClassTime.day1, " +
                "ClassTime.day2, " +
                "ClassTime.day3, " +
                "ExamTime.eStart, " +
                "ExamTime.eEnd" +
                " FROM %s AND Course.name LIKE %%?%% OR CourseGroup.instructor LIKE %%?%%);", COURSES_JOIN);
    }

    @Override
    protected void fillFindBySearchValues(PreparedStatement st, String search) throws SQLException {
        st.setString(1, search);
        st.setString(2, search);
    }

    @Override
    protected void fillFindByTermValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id);
    }

    @Override
    protected String getFindByTermStatement() {
        return String.format("SELECT " +
                "Course.name, " +
                "Course.code, " +
                "CourseGroup.classCode, " +
                "CourseGroup.instructor, " +
                "Course.units, " +
                "Course.cType, " +
                "CourseGroup.capacity, " +
                "CourseGroup.signedUp, " +
                "ClassTime.time, " +
                "ClassTime.day1, " +
                "ClassTime.day2, " +
                "ClassTime.day3, " +
                "ExamTime.eStart, " +
                "ExamTime.eEnd" +
                " FROM %s AND Course.cType = ?;", COURSES_JOIN);
    }

    @Override
    protected void fillFindByEmailValues(PreparedStatement st, String email) throws SQLException {

    }

    @Override
    protected String getFindByEmailStatement() {
        return null;
    }
}
