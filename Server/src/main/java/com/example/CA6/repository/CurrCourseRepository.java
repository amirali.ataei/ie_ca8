package com.example.CA6.repository;

import com.example.CA6.service.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CurrCourseRepository extends Repository<Course, String>{
    private static final String TABLE_NAME = "CurrCourse";
    private static CurrCourseRepository instance;
    private static final String COURSES_JOIN = "Course, " +
            "CourseGroup, " +
            "ClassTime, " +
            "ExamTime, " +
            "CurrCourse, " +
            "Student " +
            "WHERE " +
            "Course.code = CourseGroup.code And " +
            "CourseGroup.classCode = ClassTime.classCode And " +
            "CourseGroup.classCode = ClassTime.classCode And " +
            "ClassTime.code = ExamTime.code And " +
            "ClassTime.classCode = ExamTime.classCode And " +
            "CurrCourse.sId = Student.id And ";

    public static CurrCourseRepository getInstance() {
        if (instance == null) {
            try {
                instance = new CurrCourseRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in CourseRepository.create query.");
            }
        }
        return instance;
    }

    private CurrCourseRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s (\n" +
                        "  `sId` char(225) NOT NULL,\n" +
                        "  `cCode` char(225) NOT NULL,\n" +
                        "  `cClassCode` char(225) NOT NULL,\n" +
                        "  `isWaiting` tinyint DEFAULT NULL,\n" +
                        "  `finalized` tinyint DEFAULT NULL,\n" +
                        "  PRIMARY KEY (`sId`,`cCode`,`cClassCode`),\n" +
                        "  KEY `code1_idx` (`cCode`,`cClassCode`),\n" +
                        "  CONSTRAINT `code2` FOREIGN KEY (`cCode`, `cClassCode`) REFERENCES `CourseGroup` (`code`, `classCode`) ON DELETE CASCADE,\n" +
                        "  CONSTRAINT `sId2` FOREIGN KEY (`sId`) REFERENCES `Student` (`id`) ON DELETE CASCADE\n" +
                        ");", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT DISTINCT " +
                "Course.name, " +
                "Course.code, " +
                "CourseGroup.classCode, " +
                "CourseGroup.instructor, " +
                "Course.units, " +
                "Course.cType, " +
                "CourseGroup.capacity, " +
                "CourseGroup.signedUp, " +
                "ClassTime.time, " +
                "ClassTime.day1, " +
                "ClassTime.day2, " +
                "ClassTime.day3, " +
                "ExamTime.eStart, " +
                "ExamTime.eEnd" +
                " FROM %s CurrCourse.sId = ?;", COURSES_JOIN);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s(`sId`, cCode, cClassCode, isWaiting, finalized) Select ?, ?, ?, ?, ? Where not exists(Select * from %s where CurrCourse.`sId` = ? AND CurrCourse.cCode = ? AND cClassCode = ?);", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Course data) throws SQLException {

    }

    @Override
    protected void fillInsertByIdValues(PreparedStatement st, Course data, String id) throws SQLException {
        st.setString(1, id);
        st.setString(2, data.getCode());
        st.setString(3, data.getClassCode());
        st.setBoolean(4, data.getStatus());
        st.setBoolean(5, data.isWaiting());
        st.setString(6, id);
        st.setString(7, data.getCode());
        st.setString(8, data.getClassCode());
    }

    @Override
    protected String getFindBySearch() {
        return null;
    }

    @Override
    protected void fillFindBySearchValues(PreparedStatement st, String search) throws SQLException {

    }

    @Override
    protected void fillFindByTermValues(PreparedStatement st, String id) {

    }

    @Override
    protected String getFindByTermStatement() {
        return null;
    }

    @Override
    protected void fillFindByEmailValues(PreparedStatement st, String email) throws SQLException {

    }

    @Override
    protected String getFindByEmailStatement() {
        return null;
    }

    @Override
    protected String getFindAllStatement() {
        return null;
    }

    @Override
    protected void fillFindBySearchTypeValues(PreparedStatement st, String search, String type) throws SQLException {

    }

    @Override
    protected String getFindBySearchType() {
        return null;
    }

    @Override
    protected Course convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new Course(rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getInt(5),
                rs.getString(6),
                rs.getInt(7),
                rs.getInt(8),
                rs.getString(9),
                rs.getString(10),
                rs.getString(11),
                rs.getString(12),
                rs.getString(13),
                rs.getString(14));
    }

    @Override
    protected ArrayList<Course> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Course> courses = new ArrayList<>();
        while (rs.next()) {
            courses.add(this.convertResultSetToDomainModel(rs));
        }
        return courses;
    }
}
