package com.example.CA6.service;

import static com.example.CA6.util.Tools.courses;

public class MinJob implements Runnable {
    @Override
    public void run(){
        System.out.println("Hi");
        for(com.example.CA6.service.Course course : courses) {
            while(course.getWaitingStudentsSize() > 0 && course.getSignedUp() < course.getCapacity()){
                course.popWaiting();
            }
        }
    }
}
