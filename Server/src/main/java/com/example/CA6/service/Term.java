package com.example.CA6.service;

import com.example.CA6.repository.PrevCourseRepository;

import java.sql.SQLException;
import java.util.ArrayList;

import static com.example.CA6.util.Tools.studentId;

public class Term {
    private ArrayList<com.example.CA6.service.Grade> grades = new ArrayList<>();
    private double avg;
    private int number;

    private final PrevCourseRepository prevCourseRepository = PrevCourseRepository.getInstance();

    Term(int _number) {
        number = _number;
        avg = 0.0;
    }

    public ArrayList<com.example.CA6.service.Grade> getGrades() {
        String s1 = String.valueOf(number);
        String s2 = String.valueOf(studentId);
        try {
            System.out.println(s1 + "-" + s2);
            this.grades = (ArrayList<Grade>) prevCourseRepository.findAllByTerm(s1 + "-" + s2);
//            return (ArrayList<Grade>) prevCourseRepository.findAllByTerm(s1 + "-" + s2);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return grades;
    }

    public void addGrade(com.example.CA6.service.Grade grade){
        grades.add(grade);
    }

    public double getAvg() {
        avg = 0;
        int units = 0;
        for(com.example.CA6.service.Grade grade: grades){
            avg += (grade.getGrade() * grade.getUnits());
            units += grade.getUnits();
        }
        avg /= units;
        return avg;
    }

    public int getNumber() {
        return number;
    }

}
