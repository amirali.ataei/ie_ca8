package com.example.CA6.util;

import com.example.CA6.exception.OfferingNotFoundException;
import com.example.CA6.exception.StudentNotFoundException;
import com.example.CA6.repository.*;
import com.example.CA6.service.Course;
import com.example.CA6.service.Grade;
import com.example.CA6.service.Student;
import com.google.gson.Gson;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class Tools {
    public static ArrayList<com.example.CA6.service.Student> students;
    public static ArrayList<com.example.CA6.service.Course> courses;

    public static String studentId = "";
    public static ArrayList<com.example.CA6.service.Course> selectedCourses = new ArrayList<>();
    public static ArrayList<com.example.CA6.service.Course> lastSubmit = new ArrayList<>();
    public static String searchFilter = "";

    private static final CourseRepository courseRepository = CourseRepository.getInstance();
    private static final CourseGroupRepository courseGroupRepository = CourseGroupRepository.getInstance();
    private static final ClassTimeRepository classTimeRepository = ClassTimeRepository.getInstance();
    private static final ExamTimeRepository examTimeRepository = ExamTimeRepository.getInstance();
    public static final PrerequisitesRepository prerequisitesRepository = PrerequisitesRepository.getInstance();
    private static final StudentRepository studentRepository = StudentRepository.getInstance();
    private static final PrevCourseRepository prevCourseRepository = PrevCourseRepository.getInstance();


    public static ArrayList<com.example.CA6.service.Course> getCourses() throws Exception {
        URL url = new URL("http://138.197.181.131:5200/api/courses");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        ArrayList<com.example.CA6.service.Course> courses = new ArrayList<com.example.CA6.service.Course>();

        int status = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            Object obj = JSONValue.parse(inputLine);
            JSONArray array = (JSONArray)obj;
            for(Object object : array)
            {
                JSONObject jsonObject = (JSONObject) object;
                Gson gson = new Gson();
                Course course = gson.fromJson(jsonObject.toJSONString(), com.example.CA6.service.Course.class);
                courses.add(course);
                courseRepository.insert(course);
                courseGroupRepository.insert(course);
                classTimeRepository.insert(course);
                examTimeRepository.insert(course);
            }
        }
        in.close();
        con.disconnect();
        return courses;
    }

    public static ArrayList<com.example.CA6.service.Student> getStudents() throws Exception {
        URL url = new URL("http://138.197.181.131:5200/api/students");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        ArrayList<com.example.CA6.service.Student> students = new ArrayList<com.example.CA6.service.Student>();

        int status = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            Object obj = JSONValue.parse(inputLine);
            JSONArray array = (JSONArray) obj;
            for(Object object : array)
            {
                JSONObject jsonObject = (JSONObject) object;
                Gson gson = new Gson();
                Student student = gson.fromJson(jsonObject.toJSONString(), com.example.CA6.service.Student.class);
                students.add(student);
                studentRepository.insert(student);
            }
        }
        in.close();
        con.disconnect();
        return students;
    }

    public static ArrayList<com.example.CA6.service.Grade> getGrades(String student_id) throws Exception {
        URL url = new URL("http://138.197.181.131:5200/api/grades/" + student_id);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        ArrayList<com.example.CA6.service.Grade> grades = new ArrayList<com.example.CA6.service.Grade>();

        int status = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            Object obj = JSONValue.parse(inputLine);
            JSONArray array = (JSONArray) obj;
            for(Object object : array)
            {
                JSONObject jsonObject = (JSONObject) object;
                Gson gson = new Gson();
                Grade grade = gson.fromJson(jsonObject.toJSONString(), com.example.CA6.service.Grade.class);
                grades.add(grade);
                prevCourseRepository.insertById(grade, student_id);
            }
        }
        in.close();
        con.disconnect();
        return grades;
    }

    public static com.example.CA6.service.Course getCourse(String code, String classCode) throws Exception {
        return courseRepository.findById(code + "-" + classCode);

    }

    public static com.example.CA6.service.Student getStudent(String studentId) throws Exception {
        return studentRepository.findById(studentId);
    }

    public static ArrayList<com.example.CA6.service.Course> filter() {
        ArrayList<com.example.CA6.service.Course> filterCourses = new ArrayList<>();
        for(com.example.CA6.service.Course course : courses) {
            if(course.getName().contains(searchFilter))
                filterCourses.add(course);
        }
        return filterCourses;
    }
}
